FactoryGirl.define do
  factory :product do
    name 					 "Name"
		description_en "Description"
		description_pt "Descrição"
		boxshot 			 "https://upload.wikimedia.org/wikipedia/en/7/76/Grim_Fandango_artwork.jpg"

		trait :action do
			category "action"
		end

		trait :adventure do
			category "adventure"
		end

		trait :shooter do
			category "shooter"
		end

		trait :horror do
			category "horror"
		end

		trait :published do
			published true
		end

		trait :unpublished do
			published false
		end
  end
end