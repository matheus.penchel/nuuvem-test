require 'rails_helper'

RSpec.describe Product, type: :model do

  # Presence validations
  it { should validate_presence_of(:name) }
  it { should validate_presence_of(:description_en) }
  it { should validate_presence_of(:description_pt) }

  # Enums
  it "has four categories: action, adventure, shooter and horror" do
    should define_enum_for(:category).
      with([:action, :adventure, :shooter, :horror])
  end

  # Callbacks
  describe 'on initialization' do
  	it "calls increment_sku" do
      expect_any_instance_of(Product).to receive(:increment_sku)
      Product.new
  	end
  end

  # Object methods
  describe '#increment_sku' do
    before(:each) do
      product.send(:increment_sku)
    end

    context "has a sku" do
      let(:product) { FactoryGirl.create(:product, sku: 10) }

      it "doesn't change the value" do
        expect(product.sku).to eq(10)
      end
    end

    context "doesn't have a sku" do
      let(:product) { FactoryGirl.build(:product) }

      it "change the value to the greatest sku + 1" do
        expect(product.sku).to eq(Product.order(:sku).last.try(:sku).to_i + 1)
      end
    end
  end

  # Class methods
  describe '.search' do
    context "doesn't have params" do
      before(:each) do
        FactoryGirl.create_list(:product, 3)
      end

      it "doesn't break" do
        expect { Product.search }.not_to raise_error
      end

      it "returns all products" do
        expect(Product.search.count).to eq(3)
      end
    end

    context "has params" do
      before(:each) do
        FactoryGirl.create(:product, :shooter, name: "CrazyName")
        FactoryGirl.create(:product, :horror, description_en: "CrazyName")
        FactoryGirl.create(:product, :shooter, description_pt: "NormalName")
      end

      it "searchs in the description and name fields without case sensitivity" do
        params = {name: "AZY"}
        expect(Product.search(params).count).to eq(2)
      end

      it "searchs by category number" do
        params = {category: Product.categories["shooter"]}
        expect(Product.search(params).count).to eq(2)
      end

      it "searchs by both category and description/name in exclusive way" do
        params = {name: "azy", category: Product.categories["shooter"]}
        expect(Product.search(params).count).to eq(1)
      end
    end
  end

end
