# 2 Admins

User.create(email: "administrador@gmail.com", password: "mudar@123")
User.create(email: "administrator@gmail.com", password: "change@123")

# 10 Jogos
Product.create(name: "Grim Fandango", 
							 category: 1, 
							 description_en: "Walk across the realm of the dead and find your way to paradise.. Or die (again) trying.",
							 description_pt: "Atravesse o reino dos mortos e ache o seu caminho para o paraíso.. Ou morra (de novo) tentando.",
							 boxshot: "https://upload.wikimedia.org/wikipedia/en/7/76/Grim_Fandango_artwork.jpg",
							 sku: 1,
							 published: true)

Product.create(name: "Blood Omen 2", 
							 category: 0, 
							 description_en: "Rebuild your empire as Kain, the vampire legend.",
							 description_pt: "Reconstrua seu império com Kain, a lenda vampírica.",
							 boxshot: "https://upload.wikimedia.org/wikipedia/en/3/36/LoK-BloodOmen2-Cover-PC.jpg",
							 sku: 2,
							 published: true)

Product.create(name: "Counter-Strike", 
							 category: 2, 
							 description_en: "Play the world's number 1 online shooting game.",
							 description_pt: "Jogue o melhor jogo online de tiro do mundo.",
							 boxshot: "https://upload.wikimedia.org/wikipedia/pt/6/67/Counter-Strike_Box.jpg",
							 sku: 3,
							 published: true)

Product.create(name: "Dead By Daylight", 
							 category: 3, 
							 description_en: "Dead by Daylight is a multiplayer (4vs1) horror game where one player takes on the role of the 
							 savage Killer, and the other four players play as Survivors, trying to escape the Killer and avoid being caught 
							 and killed.",
							 description_pt: "Dead by Daylight é um jogo multilayer (4vs1) de terror onde um jogador assume o papel de um 
							 terrível Assassino, e os outros quatro jogadores jogam como os Sobreviventes, tentando escapar do Assassino e 
							 de serem encontrados e mortos.",
							 boxshot: "http://www.digiseller.ru/preview/543792/p1_2131943_e62aa6b6.png",
							 sku: 4,
							 published: true)

Product.create(name: "The Secret of Monkey Island", 
							 category: 1, 
							 description_en: "Discover the secret of Monkey Island as Guybrush Threepwood, a mighty pirate.",
							 description_pt: "Descubra o segredo da Ilha dos Macacos com Guybrush Threepwood, um poderoso pirata.",
							 boxshot: "https://upload.wikimedia.org/wikipedia/en/a/a8/The_Secret_of_Monkey_Island_artwork.jpg",
							 sku: 5,
							 published: true)

Product.create(name: "Full Throttle", 
							 category: 1, 
							 description_en: "When I'm on the road, I'm indestructible.",
							 description_pt: "Quando eu estou na estrada, sou indestrutível.",
							 boxshot: "https://upload.wikimedia.org/wikipedia/en/c/c2/Full_Throttle_artwork.jpg",
							 sku: 6,
							 published: true)

Product.create(name: "Dead Space", 
							 category: 3, 
							 description_en: "Your ship is broken and to survive you'll have to discover what happened to an abandoned 
							 miner's ship crew.",
							 description_pt: "Sua nave quebrou e para sobreviver você terá que descobrir o que aconteceu com a 
							 tripulação de uma nave mineiradora abandonada.",
							 boxshot: "https://upload.wikimedia.org/wikipedia/en/5/57/Dead_Space_Box_Art.jpg",
							 sku: 7,
							 published: true)

Product.create(name: "Call of Juarez", 
							 category: 2, 
							 description_en: "Control Billy 'Candle' and Ray McCall in a wild west trama.",
							 description_pt: "Controle Billy 'Candle' e Ray McCall em uma trama no velho oeste.",
							 boxshot: "https://upload.wikimedia.org/wikipedia/en/5/53/Call_of_Juarez.jpg",
							 sku: 8,
							 published: true)

Product.create(name: "Attack on Titan", 
							 category: 0, 
							 description_en: "Revive the anime in this bloodthirsty action game.",
							 description_pt: "Reviva o anime neste sanguinolento jogo de ação.",
							 boxshot: "http://i.imgur.com/KY0iYEQ.jpg",
							 sku: 9,
							 published: true)

Product.create(name: "Trine", 
							 category: 0, 
							 description_en: "Save the realm as an archer, wizard or warrior.",
							 description_pt: "Salve o reino como um arqueiro, um mago ou um guerreiro.",
							 boxshot: "https://upload.wikimedia.org/wikipedia/en/b/b2/Trine.png",
							 sku: 10,
							 published: true)