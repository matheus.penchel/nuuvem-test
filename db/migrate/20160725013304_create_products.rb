class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.text :description_en, null: false
      t.text :description_pt, null: false
      t.string :boxshot
      t.integer :category, null: false, default: 0
      t.integer :sku, null: false
      t.boolean :published, null: false, default: false

      t.timestamps null: false
    end
  end
end
