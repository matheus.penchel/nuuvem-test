Rails.application.routes.draw do

  devise_for :users
  root to: 'products#index'

  namespace :admin do
    resources :products
  end

  resources :products, only: [:index, :show]

  get '/change_locale/:locale', to: 'settings#change_locale', as: :change_locale

end
