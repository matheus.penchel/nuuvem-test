class ApplicationController < ActionController::Base

  protect_from_forgery with: :exception

	before_action :set_locale

	# ==================================================================
	# O idioma do site estava variando de acordo com a URL, mas troquei
	# por conta dos requisitos do desafio. De acordo com o Rails guides,
	# o ideal seria deixar na URL para tornar o processo transparente.
	#
	# Por exemplo, se alguem mandasse a URL do site para um amigo, ele
	# veria exatamente o mesmo conteúdo; isso não acontece quando
	# usamos cookies.
	# ==================================================================

	def set_locale
	  if cookies[:educator_locale] && I18n.available_locales.include?(cookies[:educator_locale].to_sym)
	    locale = cookies[:educator_locale].to_sym
	  else
	    locale = I18n.default_locale
	    cookies.permanent[:educator_locale] = locale
	  end

	  I18n.locale = locale
	end
end
