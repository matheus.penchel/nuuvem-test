module ProductsHelper
	def translated_product_categories
		Product.categories.map { |key, value| [translated_product_category(key), value] }.to_h
	end

	def translated_product_category(category)
		I18n.t :"product_categories.#{category}"
	end
end
