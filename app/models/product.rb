class Product < ActiveRecord::Base
	enum category: [ :action, :adventure, :shooter, :horror ]

	validates_presence_of :name, :description_en, :description_pt

	after_initialize :increment_sku

	validate :both_description_fields_required

	def self.search(search={})
		result = Product.all

		result = result.where("lower(name) LIKE :search or 
													 lower(description_en) LIKE :search or 
													 lower(description_pt) LIKE :search", search: "%#{search[:name].downcase}%") if search[:name].present?
		result = result.where(category: search[:category]) if search[:category].present?

		result
	end

	private
	def increment_sku
		self.sku ||= Product.order(:sku).last.try(:sku).to_i + 1
	end

  def both_description_fields_required
  	reminder_message = "#{I18n.t :remember}"
    errors.add(:description_en, reminder_message) if description_pt.blank?
    errors.add(:description_pt, reminder_message) if description_en.blank?
  end
end
